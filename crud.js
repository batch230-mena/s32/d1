let http = require("http");

// Mock database

let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@mail.com"
	}
];

http.createServer(function (request, response){

	// Route for returning all items upon receiving GET request
	if(request.url == "/users" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'application/json'});

		// Input has to be a data type of string, hence the JSON.stringify() method.
		// This string input will be converted to desired output data type which has been set to JSON
		// This is done because requests and response sent between client and a node JS server requires the information to be sent and received as a stringified.
		response.write(JSON.stringify(directory));
		console.log(directory);
		response.end();
	}

	if(request.url == "/users" && request.method == "POST"){

		let requestBody = '';

		// A stream is a sequence of data (in our case it will be our per data that will be stored to the requestBody variable)
		
		// Here we read the data submitted from Postman or from our client
		request.on('data', function(data){
			console.log(data);
			requestBody += data;
		});

		// response end step - only runs after the request has been completely been set
		request.on('end', function(){
			//console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}

			directory.push(newUser);

			response.writeHead(200, {'Content-Type' : 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}

}).listen(4000);

console.log("Server is running at localhost: 4000");